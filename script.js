/*
	Operators
		- Arithmetic Operators
		- Assignment Operator (=)
		- Compound Assignment Operators

		- Comparison Operators
		- Logical Operators

*/

/* Arithmetic Operators
	- perform arithmetic operations on numerical values (or variables) as their operands and returns a single numerical value

	Operators		Description
		+			addition
		-			subtraction
		*			multiplication
		/			division
		%			modulus
		++			increment
		--			decrement

	*Operand - numbers/ variables in an expression
	*Operator - operation to be done in the operands

	Example:
*/
	// let m = 20;
	// let n = 10;
	// let z = 100;
	// let a = 3;

	//console.log(m + n);

	//console.log(m - n);

	//console.log(m * n);

	//console.log(m / n);

	//console.log(z % a);  1
	
//Increment (++) & Decrement (--)

	//let m = 20;
	//let n = 10;

	// operator comes first before variable
		// it shows the value right away
	// console.log(++m);
	// console.log(--n);

	// let o = 15;
	// let p = 25;

	// variable comes first before operator
		//display the initial value 
		// returns the new value
	// console.log(o++);
	// console.log(o);
	// console.log(o);
	// console.log(p--);

	// console.log((++o) + p);

/*
	Compound Assignment Operators
		- shorthand for arithmetic and assignment operation
		- performing arithmetic operation while assigning the return value (result) back to the variable.

		Operators	Description
			=		assignment
			+=		addition assignment
			-=		subtraction assignment
			*=		multiplication assignment
			/=		division assignment



		// addition-assignment
*/		let m = 20;
		console.log(m += 7);
		// 20 + 7 = 27(result); arithmetic
		// m = 27;	assignment
		// same as: m = m + 7
		console.log(m);

		// subtraction-assignment
		m = 20;
		console.log(m -= 7);
		// 20 - 7 = 13
		// m = 13
		console.log(m);

		// multiplication-assignment
		m = 20;
		console.log(m *= 5);
		console.log(m);

		// division-assignment
		m = 20;
		console.log(m /= 4);
		console.log(m);

		// modulus-assignment
		m = 20;
		console.log(m %= 3);

/*
	Mini Activity (to be answered after the break)

	Using console.log(), perform all Compound Assignment Operations using the variable below:

*/
	m = 35;
	//you may supply the other operand's value



